<?php

namespace Language;

/**
 * Business logic related to generating language files.
 */
class LanguageBatchBo
{
    /**
     * Contains the applications which ones require translations.
     *
     * @var array
     */
    public static $applications = array();

    /**
     * Starts the language file generation.
     *
     * @return void
     */
    public static function generateLanguageFiles()
    {
        self::$applications = Config::get('system.translated_applications');

        self::generateText();
        foreach (self::$applications as $application => $languages)
        {
                    self::appText($application);
                    foreach ($languages as $language)
                    {
                                self::langText($language);

                                try
                                {
                                    if (self::getLanguageFile($application, $language))
                                    {
                                        self::okText();
                                    }
                                    else
                                    {
                                        throw new \Exception('Unable to generate language file!');
                                    }
                                }
                                catch (\Exception $e)
                                {
                                    echo "<div style='font-family:monospace; width: 30%; background: #55474e; color:white; text-align: center;'>".$e->getMessage()."</div>";
                                }
                    }
        }
    }
    public static function generateText()
    {
    echo "<div style='font-family:arial; width: 30%; background: #dfdbdb; text-align: center; padding: 20px 0 20px 0; border-bottom: solid; border-color: #0d112b'>"."\nGenerating language files\n"."<br>"."</div>";
    }
    public static function appText($application)
    {
    echo "<div style='font-family: monospace; background: #a70000; color: white; width: 30%; text-align: center; padding: 15px 0 15px 0; font-weight: 500;'>"."APPLICATION: " . $application . "\n"."<br>"."</div>";
    }
    public static function langText($language)
    {
    echo "<div style='font-family: monospace; background: #b6e3fb; width: 30%; text-align: center;'>"."\t[LANGUAGE: " . $language . "]"."<br>"."</div>";
    }
    public static function okText()
    {
    echo "<div style='background: #2ea737; width: 30%; color: white; text-align: center'>"."Language File Generated! Status: OK\n"."<br>"."</div>";
    }

    /**
     * Gets the language file for the given language and stores it.
     *
     * @param string $application The name of the application.
     * @param string $language The identifier of the language.
     *
     * @throws CurlException   If there was an error during the download of the language file.
     *
     * @return bool   The success of the operation.
     */
   private static function apiCallGetLanguageFile($application, $language)
   {
        $languageResponse = ApiCall::call(
        'system_api',
        'language_api',
        array(
        'system' => 'LanguageFiles',
        'action' => 'getLanguageFile'
        ),
        array('language' => $language)
        );

                    try
                    {
                        self::checkForApiErrorResult($languageResponse);
                    }
                    catch (\Exception $e)
                    {
                        echo "<div style='font-family:monospace; width: 30%; background: #0d112b; color:white; text-align: center;'>".$e->getMessage()."</div>";
                    }
                    return $languageResponse;
    }

    protected static $_result = false;

    public static $destination;

    protected static function getLanguageFile($application, $language)
    {
            self::$_result;

            $destination = self::getLanguageCachePath($application) . $language . '.php';

            self::$destination = $destination;
            //for test occasion
            self::destText($destination);

                    if (!is_dir(dirname($destination)))
                    {
                        mkdir(dirname($destination), 0755, true);
                    }
                    $_result = file_put_contents($destination, self::apiCallGetLanguageFile($application, $language)['data']);

                    return (bool)$_result;
    }

    protected static function destText($destination)
    {
        echo "<div style='font-family: monospace; color: #6e7274; font-size: 11px;'>".$destination."</div>";
    }
	/**
	 * Gets the directory of the cached language files.
	 *
	 * @param string $application   The application.
	 *
	 * @return string   The directory of the cached language files.
	 */
	public static function getLanguageCachePath($application)
	{
		return Config::get('system.paths.root') . '/cache/' . $application. '/';
	}

	/**
	 * Gets the language files for the applet and puts them into the cache.
	 *
	 * @throws Exception   If there was an error.
	 *
	 * @return void
	 */




	public static function generateAppletLanguageXmlFiles()
{
		$applets = array('memberapplet' => 'JSM2_MemberApplet',);

        self::getAppletLangText();

            foreach ($applets as $appletDirectory => $appletLanguageId)
            {
                        self::gettingAppletText($appletLanguageId, $appletDirectory);

                        $languages = self::getAppletLanguages($appletLanguageId);
                        try
                        {
                                if (empty($languages))
                                {
                                throw new \Exception('There is no available language for the ' . $appletLanguageId . ' applet.');
                                }
                                else
                                {
                                self::availableLangText($languages);
                                }

                                $path = self::getAppletDir();

                                    foreach ($languages as $language)
                                    {

                                                $xmlContent = self::getAppletLanguageFile($appletLanguageId, $language);

                                                $xmlFile = self::makeAppletXml($path, $language);

                                                try
                                                {
                                                    if (strlen($xmlContent) == file_put_contents($xmlFile, $xmlContent))
                                                    {
                                                        self::okXmlApplet($xmlFile);
                                                    }
                                                    else
                                                    {
                                                        throw new \Exception('Unable to save applet: (' . $appletLanguageId . ') language: (' . $language
                                                        . ') xml (' . $xmlFile . ')!');
                                                    }
                                                }
                                                catch (\Exception $excep)
                                                {
                                                    echo $excep->getMessage();
                                                }
                                    }

                                    self::xmlCachedOk($appletLanguageId, $appletDirectory);
                                    echo "<div style='width: 30%; background: #3a9265; font-family: monospace; text-align: center'>" . "\nApplet language XMLs generated.\n";
                        }
                        catch (\Exception $ex)
                        {
                            echo "<div style='width: 30%; background: #363e70; color: white; font-family: monospace; text-align: center'>".$ex->getMessage()."</div>";
                        }

            }
}

    protected static function getAppletLangText()
    {
        echo "<div style='width: 30%; background: #feffe6; padding: 20px 0 20px 0; border-bottom: solid; border-color: #0d112b; font-family: arial; text-align: center'>"."\nGetting applet language XMLs...\n"."<br>"."</div>";
    }
    protected static function gettingAppletText($appletLanguageId, $appletDirectory)
    {
        echo "<div style='font-family:monospace; width: 30%; background: #dfdbdb; text-align: center; padding: 10px 0 10px 0;'>"." Getting > $appletLanguageId ($appletDirectory) language xmls..\n"."<br>"."</div>";
    }

    protected static function availableLangText($languages)
    {
        echo "<div style='font-family: monospace; background: #b6e3fb; width: 30%; text-align: center;'>".'Available languages: ' . implode(', ', $languages) . "\n"."<br>"."</div>";
    }
    protected static function getAppletDir()
    {
        return Config::get('system.paths.root') . '/cache/flash';
    }

    protected static function makeAppletXml($path, $language)
    {
        return $path . '/lang_' . $language . '.xml';
    }

    protected static function okXmlApplet($xmlFile)
    {
        echo "<div style='background: green; width: 30%; color: white; text-align: center; display: block; word-break: break-word;'>"."Saving: $xmlFile"."<br> was successful.\n"."<br>"."</div>";
    }

    protected static function xmlCachedOk($appletLanguageId, $appletDirectory)
    {
        echo "<div style='background: #aa0000; text-align: center; color: white; width: 30%;'>" . "LiveJasmin &nbsp" . "$appletLanguageId ($appletDirectory) language xml cached.\n"."<br>"."</div>";
    }
    /**
     * Gets the available languages for the given applet.
     *
     * @param string $applet   The applet identifier.
     *
     * @return array   The list of the available applet languages.
     */
protected static function getAppletLanguages($applet)
{
		$result = ApiCall::call(
                'system_api',
                'language_api',
                array(
                    'system' => 'LanguageFiles',
                    'action' => 'getAppletLanguages'
                  ),
                array('applet' => $applet)
                  );

                        try
                        {
                            self::checkForApiErrorResult($result);
                        }
                        catch (\Exception $e)
                        {
                            $e->getMessage();
                        }

                        return $result['data'];
}

	/**
	 * Gets a language xml for an applet.
	 *
	 * @param string $applet      The identifier of the applet.
	 * @param string $language    The language identifier.
	 *
	 * @return string|false   The content of the language file or false if weren't able to get it.
	 */
protected static function getAppletLanguageFile($applet, $language)
{
		$result = ApiCall::call(
			'system_api',
			'language_api',
			array(
				'system' => 'LanguageFiles',
				'action' => 'getAppletLanguageFile'
			),
			array(
				'applet' => $applet,
				'language' => $language
			)
		);

                try
                {
                    self::checkForApiErrorResult($result);
                }
                catch (\Exception $e)
                {
                    echo "<div style='font-family: monospace; font-weight: bold; color: white; background: #f12e45; width: 30%; text-align: center;'>".$e->getMessage()."</div>";
                    throw new \Exception('Getting language xml for applet: (' . $applet . ') on language: (' . $language . ') was unsuccessful: ');
                }

                return $result['data'];
}

	/**
	 * Checks the api call result.
	 *
	 * @param mixed  $result   The api call result to check.
	 *
	 * @throws Exception   If the api call was not successful.
	 *
	 * @return void
	 */
	protected static function checkForApiErrorResult($result)
	{
		// Error during the api call.
		if ($result === false || !isset($result['status'])) {
			throw new \Exception('Error during the api call');
		}
		// Wrong response.
		if ($result['status'] != 'OK') {
			throw new \Exception('Wrong response: '
				. (!empty($result['error_type']) ? 'Type(' . $result['error_type'] . ') ' : '')
				. (!empty($result['error_code']) ? 'Code(' . $result['error_code'] . ') ' : '')
				. ((string)$result['data']));
		}
		// Wrong content.
		if ($result['data'] === false) {
			throw new \Exception('Wrong content!');
		}
	}
}
