<?php
require 'LanguageBatchBo.php';
require 'Config.php';
require 'ApiCall.php';


class ExceptionTest extends PHPUnit_Framework_TestCase{
        public $test;

        public function setUp()
        {
            $this->test  = new Language\LanguageBatchBo;
        }

    public function testTheApplicationsAttribute()
    {
         $this->expectOutputString(Language\LanguageBatchBo::$applications);
         Language\Config::get('system.translated_applications');
    }

    public function testGenerateLangFileText()
    {
        $this->expectOutputString(Language\LanguageBatchBo::generateText());
        echo "\nGenerating language files\n"."<br>"."<br>";
    }

    public function testGenerateAppText()
    {
        $application = "";
        $this->expectOutputString(Language\LanguageBatchBo::appText($application));
        echo "[APPLICATION: " . $application . "]\n"."<br>";
    }
    public function testGenerateLanguageFileOutputTextTest()
    {
        $application = "";
        $this->expectOutputString(Language\LanguageBatchBo::generateLanguageFiles());
        echo "[APPLICATION: " . $application . "]\n"."<br>";
    }

    public function testGenerateLangText()
    {
        $language = "";
        $this->expectOutputString(Language\LanguageBatchBo::langText($language));
        echo "\t[LANGUAGE: " . $language . "]"."<br>";
    }
    public function testOkTextGenerateLanguage()
    {
        $language = "";
        $this->expectOutputString(Language\LanguageBatchBo::okText());
        echo "\t[LANGUAGE: " . $language . "]"."<br>";
    }

    public function testGenerateLanguageFiles()
    {
        $this->expectOutputString(Language\LanguageBatchBo::generateLanguageFiles());
        Language\LanguageBatchBo::okText();
    }

    public function testIfPhpFileExists()
    {
        $this->assertFileExists(\Language\LanguageBatchBo::$destination, $message = "Super, the file exists!");
        echo $message;
    }

    public function testApplicationsAttributeExistsInObject()
    {
        $this->assertObjectHasAttribute("applications", new Language\LanguageBatchBo);
    }
    public function testDestinationAttributeExistsInObject()
    {
        $this->assertObjectHasAttribute("destination", new Language\LanguageBatchBo);
    }
    public function testApplicationsStaticAttributeExistsInObject()
    {
        $this->assertClassHasStaticAttribute("applications", 'Language\LanguageBatchBo');
    }
    public function testDestinationStaticAttributeExistsInObject()
    {
        $this->assertClassHasStaticAttribute("destination", 'Language\LanguageBatchBo');
    }




}
